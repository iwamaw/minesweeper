package com.example.demo.services;

import com.example.demo.models.Board;
import com.example.demo.models.Coordinate;

import static com.example.demo.utils.Constants.*;
import static com.example.demo.utils.Validator.*;

import org.springframework.stereotype.Service;
import com.example.demo.services.common.BoardService;
import com.example.demo.repositories.common.BoardRepository;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.List;
import java.util.Random;

@Service
public class BoardServiceImpl implements BoardService {

    private BoardRepository boardRepository;
    private Random random;

    @Autowired
    public BoardServiceImpl(BoardRepository boardRepository) {
        this.boardRepository = boardRepository;
        this.random = new Random();
    }


    @Override
    public Board createEmptyBoard(int size) {
        checkSize(size);
        return boardRepository.createEmptyBoard(size);
    }

    @Override
    public Board createBoard(int size, int numberOfMines) {
        checkSize(size);
        checkNumberOfMines(numberOfMines);
        return boardRepository.createBoard(size, numberOfMines);
    }

    public boolean makeMove(Board realBoard, Board playBoard, int row, int col) {

        checkNotNull(realBoard);
        checkNotNull(playBoard);
        checkCoordinate(row, col);

        if (!realBoard.getUnlucky()) {
            realBoard.setUnlucky(true);
            if (realBoard.getMatrix()[row][col] == MINE) {
                fixUnluckyFirstStep(realBoard, row, col);
            }
        }

        if (realBoard.getMatrix()[row][col] == MINE) {
            playBoard.setMatrix(fillMatrixWithMines(playBoard.getMatrix(), realBoard.getMines()));
            return true;
        }

        markAdjacentSafeCells(realBoard, playBoard, row, col);
        return false;
    }

    private void markAdjacentSafeCells(Board realBoard, Board playBoard, int row, int col) {

        if (playBoard.getMatrix()[row][col] != UNVISITED) {
            return;
        }

        char[][] realMatrix = realBoard.getMatrix();
        char[][] playMatrix = playBoard.getMatrix();
        int countMines = checkAdjacentCells(realMatrix, row, col);

        playMatrix[row][col] = (char) ('0' + countMines);

        if (countMines == 0) {

            if (isValidPosition(row - 1, col - 1, playMatrix.length)) {
                if (!isMine(realMatrix, row - 1, col - 1)) {
                    markAdjacentSafeCells(realBoard, playBoard, row - 1, col - 1);
                }
            }

            if (isValidPosition(row - 1, col, playMatrix.length)) {
                if (!isMine(realMatrix, row - 1, col)) {
                    markAdjacentSafeCells(realBoard, playBoard, row - 1, col);
                }
            }

            if (isValidPosition(row - 1, col + 1, playMatrix.length)) {
                if (!isMine(realMatrix, row - 1, col + 1)) {
                    markAdjacentSafeCells(realBoard, playBoard, row - 1, col + 1);

                }
            }

            if (isValidPosition(row, col - 1, playMatrix.length)) {
                if (!isMine(realMatrix, row, col - 1)) {
                    markAdjacentSafeCells(realBoard, playBoard, row, col - 1);
                }
            }

            if (isValidPosition(row, col + 1, playMatrix.length)) {
                if (!isMine(realMatrix, row, col + 1)) {
                    markAdjacentSafeCells(realBoard, playBoard, row, col + 1);

                }
            }

            if (isValidPosition(row + 1, col - 1, playMatrix.length)) {
                if (!isMine(realMatrix, row + 1, col - 1)) {
                    markAdjacentSafeCells(realBoard, playBoard, row + 1, col - 1);

                }
            }

            if (isValidPosition(row + 1, col, playMatrix.length)) {
                if (!isMine(realMatrix, row + 1, col)) {
                    markAdjacentSafeCells(realBoard, playBoard, row + 1, col);
                }
            }

            if (isValidPosition(row + 1, col + 1, playMatrix.length)) {
                if (!isMine(realMatrix, row + 1, col + 1)) {
                    markAdjacentSafeCells(realBoard, playBoard, row + 1, col + 1);
                }
            }
        }
        playBoard.setMatrix(playMatrix);
    }

    private int checkAdjacentCells(char[][] matrix, int i, int j) {
        int count = 0;

        if (isValidPosition(i - 1, j - 1, matrix.length)) {
            if (isMine(matrix, i - 1, j - 1)) {
                count++;
            }
        }

        if (isValidPosition(i - 1, j, matrix.length)) {
            if (isMine(matrix, i - 1, j)) {
                count++;
            }
        }

        if (isValidPosition(i - 1, j + 1, matrix.length)) {
            if (isMine(matrix, i - 1, j + 1)) {
                count++;
            }
        }

        if (isValidPosition(i, j - 1, matrix.length)) {
            if (isMine(matrix, i, j - 1)) {
                count++;
            }
        }

        if (isValidPosition(i, j + 1, matrix.length)) {
            if (isMine(matrix, i, j + 1)) {
                count++;
            }
        }

        if (isValidPosition(i + 1, j - 1, matrix.length)) {
            if (isMine(matrix, i + 1, j - 1)) {
                count++;
            }
        }

        if (isValidPosition(i + 1, j, matrix.length)) {
            if (isMine(matrix, i + 1, j)) {
                count++;
            }
        }

        if (isValidPosition(i + 1, j + 1, matrix.length)) {
            if (isMine(matrix, i + 1, j + 1)) {
                count++;
            }
        }

        return count;
    }

    private boolean isMine(char[][] matrix, int row, int col) {
        return matrix[row][col] == MINE;
    }

    private boolean isValidPosition(int row, int col, int size) {
        return (row >= 0 && row < size) && (col >= 0 && col < size);
    }

    private char[][] fillMatrixWithMines(char[][] matrix, List<Coordinate> mines) {
        int indexMine = 0;

        while (indexMine < mines.size()) {
            matrix[mines.get(indexMine).getRow()][mines.get(indexMine).getCol()] = MINE;
            indexMine++;
        }
        return matrix;
    }

    private void fixUnluckyFirstStep(Board realBoard, int row, int col) {
        removeCoordinate(realBoard.getMines(), row, col);
        char[][] newMatrix = realBoard.getMatrix();
        newMatrix[row][col] = UNVISITED;
        int size = newMatrix.length;
        boolean replaced = false;

        while (!replaced) {
            int ran = random.nextInt(size * size);
            int x = ran / size;
            int y = ran % size;
            if (newMatrix[x][y] != MINE) {
                newMatrix[x][y] = MINE;
                replaced = true;
                realBoard.getMines().add(new Coordinate(x, y));
            }
        }
        realBoard.setMatrix(newMatrix);
    }

    private void removeCoordinate(List<Coordinate> allMines, int x, int y) {
        allMines.removeIf(coordinate -> coordinate.getRow() == x && coordinate.getCol() == y);
    }

}
