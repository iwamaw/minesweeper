package com.example.demo.services.common;

import com.example.demo.models.Board;

public interface BoardService {

    Board createBoard(int size, int numberOfMines);

    Board createEmptyBoard(int size);

    boolean makeMove(Board realBoard, Board playBoard, int row, int col);



}
