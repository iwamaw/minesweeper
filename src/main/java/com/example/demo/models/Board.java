package com.example.demo.models;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Board {

    private char[][] matrix;
    private boolean unlucky;
    private List<Coordinate> coordinatesOfMines;
//    private HashSet<Coordinate> lala;


    public Board() {
        this.unlucky = false;
    }

    public void setUnlucky(boolean unlucky) {
        this.unlucky = unlucky;
    }

    public boolean getUnlucky() {
        return this.unlucky;
    }

    public char[][] getMatrix() {
        return this.matrix;
    }

    public void setMatrix(char[][] matrix) {
        this.matrix = matrix;
    }

    public int sizeOfMatrix() {
        return matrix.length;
    }

    public void setCoordinatesOfMines(List<Coordinate> coordinatesOfMines) {
        this.coordinatesOfMines = coordinatesOfMines;
    }


    public List<Coordinate> getMines() {
        return coordinatesOfMines;
    }

    public List<String> printBoard() {
        List<String> output = new ArrayList<>();

        for (char[] chars : matrix) {
            StringBuilder string = new StringBuilder();
            for (int j = 0; j < matrix.length; j++) {
                string.append(" ").append(" ").append(chars[j]);
            }
            output.add(string.toString());
        }
        return output;
    }
}
