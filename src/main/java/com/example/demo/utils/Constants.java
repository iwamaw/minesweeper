package com.example.demo.utils;

public class Constants {

    public static final char MINE = '*';
    public static final char UNVISITED = '-';

    public static final int BOARD_SIZE_BEGINNERS = 9;
    public static final int BOARD_SIZE_INTERMEDIATE = 16;
    public static final int BOARD_SIZE_ADVANCED = 24;

    public static final int MINES_FOR_BEGINNERS_BOARD = 10;
    public static final int MINES_FOR_INTERMEDIATE_BOARD = 40;
    public static final int MINES_FOR_ADVANCED_BOARD = 99;


}
