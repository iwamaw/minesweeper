package com.example.demo.utils;

import com.example.demo.models.Board;

public class Validator {

    public static void checkSize(int size) {
        if (size <= 0) {
            throw new IllegalArgumentException("The size of the board must be more than 0!");
        }
    }

    public static void checkNumberOfMines(int numberOfMines) {
        if (numberOfMines < 0) {
            throw new IllegalArgumentException("The number of mines should be positive number!");
        }
    }

    public static void checkNotNull(Board board) {
        if (board == null) {
            throw new NullPointerException("Must pass a valid board!");

        }
    }

    public static void checkCoordinate(int row, int col) {
        if (row < 0 || col < 0) {
            throw new IllegalArgumentException("The coordinates must be positive numbers!");
        }
    }
}
