package com.example.demo.repositories;

import com.example.demo.models.Board;
import com.example.demo.models.Coordinate;
import org.springframework.stereotype.Repository;

import static com.example.demo.utils.Constants.*;

import com.example.demo.repositories.common.BoardRepository;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.List;
import java.util.Random;
import java.util.ArrayList;

@Repository
public class BoardRepositoryImpl implements BoardRepository {

    @Autowired
    public BoardRepositoryImpl() {
    }

    @Override
    public Board createEmptyBoard(int size) {

        Board board = new Board();
        char[][] matrix = new char[size][size];

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                matrix[i][j] = UNVISITED;
            }
        }

        board.setMatrix(matrix);
        return board;
    }

    @Override
    public Board createBoard(int size, int numberOfMines) {
        return placeMines(createEmptyBoard(size), numberOfMines);
    }

    private Board placeMines(Board board, int numberOfMines) {

        List<Coordinate> coordinatesOfMines = new ArrayList<>();
        Random randomGen = new Random();
        char[][] matrix = board.getMatrix();
        int size = board.getMatrix().length;
        int i = 0;

        boolean[] randomNumbers = new boolean[size * size];

        while (i < numberOfMines) {
            int random = randomGen.nextInt(size * size);

            if (!randomNumbers[random]) {

                int x = random / size;
                int y = random % size;

                matrix[x][y] = MINE;
                coordinatesOfMines.add(new Coordinate(x, y));
                randomNumbers[random] = true;
                i++;
            }
        }

        board.setMatrix(matrix);
        board.setCoordinatesOfMines(coordinatesOfMines);
        return board;
    }


}
