package com.example.demo.repositories.common;

import com.example.demo.models.Board;


public interface BoardRepository {

    Board createEmptyBoard(int size);

    Board createBoard(int size, int numberOfMines);

}
