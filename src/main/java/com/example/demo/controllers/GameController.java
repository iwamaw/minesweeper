package com.example.demo.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import com.example.demo.models.Board;

import static com.example.demo.utils.Constants.*;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import com.example.demo.services.common.BoardService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;


@Controller
public class GameController {

    private Board realBoard;
    private Board emptyBoard;
    private BoardService boardService;

    @Autowired
    public GameController(BoardService boardService) {
        this.boardService = boardService;
    }

    @GetMapping("/")
    public String welcome() {
        return "welcome_page";
    }

    @GetMapping("/board/{level}")
    public String game(Model model, @PathVariable String level) {
        try {
            if (level.equals("beginner")) {
                this.realBoard = boardService.createBoard(BOARD_SIZE_BEGINNERS, MINES_FOR_BEGINNERS_BOARD);
                this.emptyBoard = boardService.createEmptyBoard(BOARD_SIZE_BEGINNERS);
                model.addAttribute("board", emptyBoard);
                return "game_page";
            }
            if (level.equals("intermediate")) {
                this.realBoard = boardService.createBoard(BOARD_SIZE_INTERMEDIATE, MINES_FOR_INTERMEDIATE_BOARD);
                this.emptyBoard = boardService.createEmptyBoard(BOARD_SIZE_INTERMEDIATE);
                model.addAttribute("board", emptyBoard);
                return "game_page";
            }
            if (level.equals("advanced")) {
                this.realBoard = boardService.createBoard(BOARD_SIZE_ADVANCED, MINES_FOR_ADVANCED_BOARD);
                this.emptyBoard = boardService.createEmptyBoard(BOARD_SIZE_ADVANCED);
                model.addAttribute("board", emptyBoard);
                return "game_page";
            }
            return "endgame_page";
        } catch (IllegalArgumentException i) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, i.getMessage());
        }
    }

    @RequestMapping("/board/passCrd")
    public String getCoordinates(Model model, int row, int col) {
        try {
            if (boardService.makeMove(realBoard, emptyBoard, row, col)) {
                model.addAttribute("board", emptyBoard);
                realBoard = null;
                emptyBoard = null;
                return "endgame_page";
            }
            model.addAttribute("board", emptyBoard);
            return "game_page";
        } catch (NullPointerException n) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, n.getMessage());
        } catch (IllegalArgumentException i) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, i.getMessage());
        }
    }

}
